var clientesObtenidos;

function getClientes() {
  var url="http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  //Ll clase request tiene un evento que cambia su estado. Lo que le digo es
  //que cuando cambie el estado lance la siguiente función
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  tabla.id = "tableClientes";
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  var cabecera = document.createElement("thead");
  var cabNombre = document.createElement("th");
  cabNombre.innerText = "Nombre";
  var cabCiudad = document.createElement("th");
  cabCiudad.innerText = "Ciudad";
  var cabTelefono = document.createElement("th");
  cabTelefono.innerText = "Telefono";
  var cabPais = document.createElement("th");
  cabPais.innerText = "Pais";

  cabecera.appendChild(cabNombre);
  cabecera.appendChild(cabCiudad);
  cabecera.appendChild(cabTelefono);
  cabecera.appendChild(cabPais);

  tabla.appendChild(cabecera);

  var body = document.createElement("tbody");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var columnaTelefono = document.createElement("td");
    columnaTelefono.innerText = JSONClientes.value[i].Phone;
    var columnaPais = document.createElement("td");
    var imagenBandera = document.createElement("img");
    imagenBandera.classList.add("flag");
    var pais = JSONClientes.value[i].Country;
    var url;
    if (pais == "UK") {
      url = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
    } else {
      url = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png";
    }
    imagenBandera.src = url;
    columnaPais.appendChild(imagenBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaTelefono);
    nuevaFila.appendChild(columnaPais);
    body.appendChild(nuevaFila);
  }
  tabla.appendChild(body);
  tClientes.appendChild(tabla);
}
